#!/usr/bin/env python
# coding: utf-8

import os
import sys
import h5py
import argparse
import logging

import numpy as np
import scipy.signal as sig

import bilby
from bilby.gw.source import lal_binary_black_hole
from bilby.gw.conversion import convert_to_lal_binary_black_hole_parameters
from gwpy.timeseries import TimeSeries

from bbhnet_sim import utils

logging.basicConfig(format='%(asctime)s - %(message)s',
                    level=logging.INFO, stream=sys.stdout)

def getStrain(ifo, frame_start, frame_stop, sample_rate, high_pass=None):
    ''' Get strain from [frame_start, frame_stop) from GWOSC, resample,
    and apply high-pass filter '''

    strain = TimeSeries.fetch_open_data(ifo, frame_start, frame_stop)
    strain = strain.resample(sample_rate)
    if high_pass is not None:
        strain = strain.highpass(high_pass)
    return strain

# Parse command-line arguments
def parse_cmd():
    parser = argparse.ArgumentParser()

    # strain args: gps time
    parser.add_argument('-t0', '--frame-start', type=float, required=True,
                        help='starting GPS time of strain')
    parser.add_argument('-t1', '--frame-stop', type=float, required=True,
                        help='stopping GPS time of strain')
    parser.add_argument('-t0-psd', '--frame-start-psd', type=float, required=True,
                        help='starting GPS time of strain for PSD estimation')
    parser.add_argument('-t1-psd', '--frame-stop-psd', type=float, required=True,
                        help='stopping GPS time of strain for PSD estimation')

    # io args
    parser.add_argument('-o', '--outfile', required=True,
                        help='path to write output file in HDF5 format')

    # sim args:
    parser.add_argument('-S', '--signal', action='store_true',
                        help='Enable to add GW signal on top of background noise')
    parser.add_argument('-fs', '--sample-rate', type=float, required=False, default=1024,
                        help='sampling rate of strain')
    parser.add_argument('-fmin', '--high-pass', type=float, required=False,
                        help='frequency of highpass filter')
    parser.add_argument('-T', '--sample-duration', type=float, required=False, default=1,
                        help='duration in seconds of each sample')
    parser.add_argument('-dt', '--time-step', type=float, required=False, default=0.25,
                        help='time step size in seconds between consecutive samples')
    parser.add_argument('-p', '--prior-file', required=False,
                        help='path to prior config file. Required for signal simulation')
    parser.add_argument('--correlation-shift', type=int, required=False,
                        help='if given, also compute the correlation with given shift value')
    parser.add_argument(
        '--min-trigger', type=float, default=0.05,
        help='mininum trigger time w.r.t to sample. must be within [0, sample_duration]')
    parser.add_argument(
        '--max-trigger', type=float, default=0.95,
        help='maximum trigger time w.r.t to sample. must be within [0, sample_duration]')
    parser.add_argument('-s', '--seed', type=int, required=False,
                        help='random seed for reproducibility')

    return parser.parse_args()


if __name__ == '__main__':
    ''' Start simulation '''

    # parse command-line arguments
    FLAGS = parse_cmd()

    # compute some sim parameters from cmd input
    sample_size = int(FLAGS.sample_rate * FLAGS.sample_duration)
    step_size = int(FLAGS.sample_rate * FLAGS.time_step)
    fftlength = int(max(2, np.ceil(2048 / FLAGS.sample_rate)))

    # get strain and PSD strain from GWOSC
    logging.info('Download GW  strain from {} .. {}'.format(
        FLAGS.frame_start, FLAGS.frame_stop))
    logging.info('Download PSD strain from {} .. {}'.format(
        FLAGS.frame_start_psd, FLAGS.frame_stop_psd))
    H1_strain = getStrain(
        'H1', FLAGS.frame_start, FLAGS.frame_stop, FLAGS.sample_rate, FLAGS.high_pass)
    L1_strain = getStrain(
        'L1', FLAGS.frame_start, FLAGS.frame_stop, FLAGS.sample_rate, FLAGS.high_pass)
    H1_strain_psd = getStrain(
        'H1', FLAGS.frame_start_psd, FLAGS.frame_stop_psd,
        FLAGS.sample_rate, FLAGS.high_pass)
    L1_strain_psd = getStrain(
        'L1', FLAGS.frame_start_psd, FLAGS.frame_stop_psd,
        FLAGS.sample_rate, FLAGS.high_pass)

    # calculate the PSD and whiten noise
    H1_psd = H1_strain_psd.psd(fftlength)
    L1_psd = L1_strain_psd.psd(fftlength)
    H1_strain_whiten = H1_strain.whiten(asd=np.sqrt(H1_psd))
    L1_strain_whiten = L1_strain.whiten(asd=np.sqrt(L1_psd))

    # crop out corrupted data due to whitening
    H1_strain_whiten = H1_strain_whiten.crop(
        FLAGS.frame_start + 4, FLAGS.frame_stop - 4)
    L1_strain_whiten = L1_strain_whiten.crop(
        FLAGS.frame_start + 4, FLAGS.frame_stop - 4)

    N_sample = int((len(H1_strain_whiten) - sample_size) / step_size) + 1
    gps_start = np.arange(
        FLAGS.frame_start + 4, FLAGS.frame_start + 4 + FLAGS.time_step * N_sample ,
        FLAGS.time_step)

    # log and print out some simulation parameters
    logging.info('Simulation parameters')
    logging.info('Adding CBC signals? {}'.format(FLAGS.signal))
    logging.info('Number of samples     : {}'.format(N_sample))
    logging.info('Sample duration [sec] : {}'.format(FLAGS.sample_duration))
    logging.info('Time step [sec]       : {}'.format(FLAGS.time_step))
    logging.info('Sample rate [Hz]      : {}'.format(FLAGS.sample_rate))
    logging.info('High pass filter [Hz] : {}'.format(FLAGS.high_pass))

    if FLAGS.signal:
        logging.info('Prior file            : {}'.format(FLAGS.prior_file))
        logging.info('Trigger time          : [{}, {}]'.format(FLAGS.min_trigger, FLAGS.max_trigger))

    # set up GW signal parameters
    if FLAGS.signal:
        # define a Bilby waveform generator
        waveform_generator = bilby.gw.WaveformGenerator(
            duration=8,
            sampling_frequency=FLAGS.sample_rate,
            frequency_domain_source_model=lal_binary_black_hole,
            parameter_conversion=convert_to_lal_binary_black_hole_parameters,
            waveform_arguments={
                'waveform_approximant': 'IMRPhenomPv2',
                'reference_frequency': 50,
                'minimum_frequency': 20 },
        )

        # sample GW parameters from prior distribution
        priors = bilby.gw.prior.BBHPriorDict(FLAGS.prior_file)
        sample_params = priors.sample(N_sample)

        # sample GPS time of triggers
        triggers = np.random.uniform(FLAGS.min_trigger, FLAGS.max_trigger, N_sample)
        sample_params['geocent_time'] = triggers + gps_start

        # generate whitened GW waveforms
        H1_signals, H1_SNR = utils.generateGW(
            sample_params, FLAGS.sample_duration, triggers, 'H1',
            waveform_generator=waveform_generator, get_snr=True,
            noise_psd=H1_psd)
        L1_signals, L1_SNR = utils.generateGW(
            sample_params, FLAGS.sample_duration, triggers, 'L1',
            waveform_generator=waveform_generator, get_snr=True,
            noise_psd=L1_psd)

    # divide strain time series into overlapping chunks to create dataset
    H1_data = utils.as_stride(H1_strain_whiten, sample_size, step_size)
    L1_data = utils.as_stride(L1_strain_whiten, sample_size, step_size)
    if FLAGS.signal:
        H1_data = H1_data + H1_signals
        L1_data = L1_data + L1_signals

    # compute the Pearson coefficient if given
    if FLAGS.correlation_shift is not None:
        correlation = utils.pearson_shift(
            H1_data, L1_data, shift=FLAGS.correlation_shift)

    # Write to output file
    logging.info('Write to file {}'.format(FLAGS.outfile))
    with h5py.File(FLAGS.outfile, 'w') as f:
        # write data and ASD
        # Hanford
        H1_gr = f.create_group('H1')
        H1_dset = H1_gr.create_dataset('timeseries', data=H1_data)
        H1_dset.attrs['channel'] = 'GWOSC'
        H1_gr.create_dataset('PSD', data=H1_psd.value)
        H1_gr.create_dataset('freq', data=H1_psd.frequencies.value)

        # Livingston
        L1_gr = f.create_group('L1')
        L1_dset = L1_gr.create_dataset('timeseries', data=L1_data)
        L1_dset.attrs['channel'] = 'GWOSC'
        L1_gr.create_dataset('PSD', data=L1_psd.value)
        L1_gr.create_dataset('freq', data=L1_psd.frequencies.value)

        # if Pearson correlation is computed, also write it
        if FLAGS.correlation_shift is not None:
            gr = f.create_group('Pearson-Correlation')
            gr.create_dataset('correlation', data=correlation)

        f.create_dataset('GPS-start', data=gps_start)

        # write noise attributes
        f.attrs.update({
            'size': N_sample,
            'frame_start': FLAGS.frame_start,
            'frame_stop': FLAGS.frame_stop,
            'psd_frame_start': FLAGS.frame_start_psd,
            'psd_frame_stop': FLAGS.frame_stop_psd,
            'sample_rate': FLAGS.sample_rate,
            'sample_duration': FLAGS.sample_duration,
            'psd_fftlength': fftlength,
        })

        # write signals attributes, SNR, and signal parameters
        if FLAGS.signal:
            H1_gr.create_dataset('signal', data=H1_signals)
            L1_gr.create_dataset('signal', data=L1_signals)

            H1_gr.create_dataset('SNR', data=H1_SNR)
            L1_gr.create_dataset('SNR', data=L1_SNR)
            params_gr = f.create_group('signal_params')
            for k, v in sample_params.items():
                params_gr.create_dataset(k, data=v)
            params_gr.create_dataset('trigger-time', data=triggers)

            # Update signal attributes
            f.attrs['waveform_duration'] = 8
            f.attrs['flag'] = 'GW'
        else:
            f.attrs['flag'] = 'BACKGROUND'
